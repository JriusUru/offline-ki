# -*- coding: utf-8 -*-
""" *==LICENSE==*

CyanWorlds.com Engine - MMOG client, server and tools
Copyright (C) 2011  Cyan Worlds, Inc.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Additional permissions under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or
combining it with any of RAD Game Tools Bink SDK, Autodesk 3ds Max SDK,
NVIDIA PhysX SDK, Microsoft DirectX SDK, OpenSSL library, Independent
JPEG Group JPEG library, Microsoft Windows Media SDK, or Apple QuickTime SDK
(or a modified version of those libraries),
containing parts covered by the terms of the Bink SDK EULA, 3ds Max EULA,
PhysX SDK EULA, DirectX SDK EULA, OpenSSL and SSLeay licenses, IJG
JPEG Library README, Windows Media SDK EULA, or QuickTime SDK EULA, the
licensors of this Program grant you additional
permission to convey the resulting work. Corresponding Source for a
non-source form of such a combination shall include the source code for
the parts of OpenSSL and IJG JPEG Library used as well as that of the covered
work.

You can contact Cyan Worlds, Inc. by email legal@cyan.com
 or by snail mail at:
      Cyan Worlds, Inc.
      14617 N Newport Hwy
      Mead, WA   99021

 *==LICENSE==* """

from Plasma import *
from PlasmaTypes import *
from PlasmaConstants import *

import os.path

dynTextMap = ptAttribDynamicMap(1, "The Dynamic Texture Map")
locPath = ptAttribString(2, "Localization Name")
fontFace = ptAttribString(3, "Font Face", default="Arial")
fontSize = ptAttribInt(4, "Font Size", default=12)
fontColorR = ptAttribFloat(5, "Font Color (Red)", default=0.0)
fontColorG = ptAttribFloat(6, "Font Color (Green)", default=0.0)
fontColorB = ptAttribFloat(7, "Font Color (Blue)", default=0.0)
fontColorA = ptAttribFloat(8, "Font Color (Alpha)", default=1.0)
marginTop = ptAttribInt(9, "Margin Top", default=0)
marginLeft = ptAttribInt(10, "Margin Left", default=0)
marginBottom = ptAttribInt(11, "Margin Bottom", default=0)
marginRight = ptAttribInt(12, "Margin Right", default=0)
lineSpacing = ptAttribInt(13, "Line Spacing", default=0)
justify = ptAttribDropDownList(14, "Justification", ["left", "center", "right"])
clearColorR = ptAttribFloat(15, "Clear Color (Red)", default=0.0)
clearColorG = ptAttribFloat(16, "Clear Color (Green)", default=0.0)
clearColorB = ptAttribFloat(17, "Clear Color (Blue)", default=0.0)
clearColorA = ptAttribFloat(18, "Clear Color (Alpha)", default=0.0)

_JUSTIFY_LUT = {
    "left": PtJustify.kLeftJustify,
    "center": PtJustify.kCenter,
    "right": PtJustify.kRightJustify,
}

_LANG_LUT = {
    # English is the default, so leaving that out.
    PtLanguage.kFrench: "_french",
    PtLanguage.kGerman: "_german",
    #PtLanguage.kSpanish: "_spanish", # broken
    #PtLanguage.kItalian: "_italian", # broken
    # Purposefully leaving out Japanese due to unicode requirement - Korman assumes TPotS
    # localizations are encoded in the ACP (and that the ACP is windows-1252).
}

class xDynTextLoc(ptModifier, object):
    def __init__(self):
        ptModifier.__init__(self)
        self.id = 6745220
        self.version = 1

    def OnFirstUpdate(self):
        textMap = dynTextMap.value
        textMap.netPropagate(False)

        wrappingWidth = textMap.getWidth() - marginRight.value - marginLeft.value
        wrappingHeight = textMap.getHeight() - marginBottom.value - marginTop.value

        textMap.clearToColor(self._clearColor)
        textMap.setTextColor(self._fontColor)
        textMap.setWrapping(wrappingWidth, wrappingHeight)
        textMap.setFont(fontFace.value, fontSize.value)
        textMap.setJustify(self._justify)
        textMap.setLineSpacing(lineSpacing.value)
        textMap.drawText(marginLeft.value, marginTop.value, self.text)
        textMap.flush()

    def _clearColor_get(self):
        return ptColor(clearColorR.value, clearColorG.value, clearColorB.value, clearColorA.value)
    _clearColor = property(_clearColor_get)

    def _fontColor_get(self):
        return ptColor(fontColorR.value, fontColorG.value, fontColorB.value, fontColorA.value)
    _fontColor = property(_fontColor_get)

    def _justify_get(self):
        return _JUSTIFY_LUT.get(justify.value, PtJustify.kLeftJustify)
    _justify = property(_justify_get)

    def _text_get(self):
        language = PtGetLanguage()
        suffix = _LANG_LUT.get(language, "")
        if not suffix:
            PtDebugPrint("xDynTextLoc: No entry in language table for %d, using default (probably English)" % language, level=kWarningLevel)

        journal_path = R"ageresources\%s--%s%s.txt" % (PtGetAgeName(), locPath.value, suffix)
        if not os.path.exists(journal_path):
            PtDebugPrint("xDynTextLoc: The file for your language ('%s') appears to be missing, trying default (probably English)." % journal_path)
            journal_path = R"ageresources\%s--%s.txt" % (PtGetAgeName(), locPath.value)
            if not os.path.exists(journal_path):
                PtDebugPrint("xDynTextLoc: Default file ('%s') seems to be missing. Giving up." % journal_path)
                return "ERROR"

        try:
            fp = open(journal_path, "r")
            contents = fp.read()
            fp.close()
        except:
            PtDebugPrint("xDynTextLoc: Failed to read '%s'" % journal_path)
            contents = "ERROR"
        return contents
    text = property(_text_get)


# Copy pasta BS
glue_cl = None
glue_inst = None
glue_params = None
glue_paramKeys = None
try:
    x = glue_verbose
except NameError:
    glue_verbose = 0

def glue_getClass():
    global glue_cl
    if (glue_cl == None):
        try:
            cl = eval(glue_name)
            if issubclass(cl, ptModifier):
                glue_cl = cl
            elif glue_verbose:
                print ('Class %s is not derived from modifier' % cl.__name__)
        except NameError:
            if glue_verbose:
                try:
                    print ('Could not find class %s' % glue_name)
                except NameError:
                    print 'Filename/classname not set!'
    return glue_cl


def glue_getInst():
    global glue_inst
    if (type(glue_inst) == type(None)):
        cl = glue_getClass()
        if (cl != None):
            glue_inst = cl()
    return glue_inst


def glue_delInst():
    global glue_inst
    global glue_cl
    global glue_paramKeys
    global glue_params
    if (type(glue_inst) != type(None)):
        del glue_inst
    glue_cl = None
    glue_params = None
    glue_paramKeys = None


def glue_getVersion():
    inst = glue_getInst()
    ver = inst.version
    glue_delInst()
    return ver


def glue_findAndAddAttribs(obj, glue_params):
    if isinstance(obj, ptAttribute):
        if glue_params.has_key(obj.id):
            if glue_verbose:
                print 'WARNING: Duplicate attribute ids!'
                print ('%s has id %d which is already defined in %s' %
                      (obj.name, obj.id, glue_params[obj.id].name))
        else:
            glue_params[obj.id] = obj
    elif type(obj) == type([]):
        for o in obj:
            glue_findAndAddAttribs(o, glue_params)
    elif type(obj) == type({}):
        for o in obj.values():
            glue_findAndAddAttribs(o, glue_params)
    elif type(obj) == type(()):
        for o in obj:
            glue_findAndAddAttribs(o, glue_params)


def glue_getParamDict():
    global glue_paramKeys
    global glue_params
    if type(glue_params) == type(None):
        glue_params = {}
        gd = globals()
        for obj in gd.values():
            glue_findAndAddAttribs(obj, glue_params)
        glue_paramKeys = glue_params.keys()
        glue_paramKeys.sort()
        glue_paramKeys.reverse()
    return glue_params


def glue_getClassName():
    cl = glue_getClass()
    if (cl != None):
        return cl.__name__
    if glue_verbose:
        print ('Class not found in %s.py' % glue_name)
    return None


def glue_getBlockID():
    inst = glue_getInst()
    if (inst != None):
        return inst.id
    if glue_verbose:
        print ('Instance could not be created in %s.py' % glue_name)
    return None


def glue_getNumParams():
    pd = glue_getParamDict()
    if (pd != None):
        return len(pd)
    if glue_verbose:
        print ('No attributes found in %s.py' % glue_name)
    return 0


def glue_getParam(number):
    pd = glue_getParamDict()
    if (pd != None):
        if type(glue_paramKeys) == type([]):
            if (number >= 0) and (number < len(glue_paramKeys)):
                return pd[glue_paramKeys[number]].getdef()
            else:
                print ('glue_getParam: Error! %d out of range of attribute list' % number)
        else:
            pl = pd.values()
            if (number >= 0) and (number < len(pl)):
                return pl[number].getdef()
            elif glue_verbose:
                print ('glue_getParam: Error! %d out of range of attribute list' % number)
    if glue_verbose:
        print 'GLUE: Attribute list error'
    return None


def glue_setParam(id, value):
    pd = glue_getParamDict()
    if (pd != None):
        if pd.has_key(id):
            try:
                pd[id].__setvalue__(value)
            except AttributeError:
                if isinstance(pd[id], ptAttributeList):
                    try:
                        if type(pd[id].value) != type([]):
                            pd[id].value = []
                    except AttributeError:
                        pd[id].value = []
                    pd[id].value.append(value)
                else:
                    pd[id].value = value
        elif glue_verbose:
            print "setParam: can't find id=", id
    else:
        print 'setParma: Something terribly has gone wrong. Head for the cover.'


def glue_isNamedAttribute(id):
    pd = glue_getParamDict()
    if (pd != None):
        try:
            if isinstance(pd[id], ptAttribNamedActivator):
                return 1
            if isinstance(pd[id], ptAttribNamedResponder):
                return 2
        except KeyError:
            if glue_verbose:
                print ('Could not find id=%d attribute' % id)
    return 0


def glue_isMultiModifier():
    inst = glue_getInst()
    if isinstance(inst, ptMultiModifier):
        return 1
    return 0


def glue_getVisInfo(number):
    pd = glue_getParamDict()
    if pd != None:
        if type(glue_paramKeys) == type([]):
            if (number >= 0) and (number < len(glue_paramKeys)):
                return pd[glue_paramKeys[number]].getVisInfo()
            else:
                print ('glue_getVisInfo: Error! %d out of range of attribute list' % number)
        else:
            pl = pd.values()
            if (number >= 0) and (number < len(pl)):
                return pl[number].getVisInfo()
            elif glue_verbose:
                print ('glue_getVisInfo: Error! %d out of range of attribute list' % number)
    if glue_verbose:
        print 'GLUE: Attribute list error'
    return None
